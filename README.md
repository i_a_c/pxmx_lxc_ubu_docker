# Sample test of automation for create a ubuntu lxc from official image and install and run docker environment

## what do you need?

- proxmox VE installation
- internet connection
- a LXC/VM to use like infrastructure control center [ICC]
- ansible installed [icc]
- terraform installed [icc]

## what do I get at the end?

- 1 ubuntu LXC with a working docker environment

## what steps are necessary?

- upload on a pve node a official LXC ubutnu template
- use terraform for create 1 LXC from the modified template
- execute ansible playbook on this LXC for create a docker environment

## how to get a alpine LXC template?

- `pveam update`
- `pveam available`
- `pveam download $storage_template $template_name`

## how to use terraform?

- `terraform -chdir=terraform init`
- `terraform -chdir=terraform plan`
- `terraform -chdir=terraform apply`

## how to use ansible?

- `mv /path/to/website/files ansible/files/`
- `ansible-playbook -i ansible/ansible_hosts.txt  ansible/ansible_lxc_ubu_docker.yaml`
